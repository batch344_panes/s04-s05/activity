import java.sql.Array;
import java.util.ArrayList;

public class ContactActions implements Actions{
    @Override
    public void getDetails(Phonebook phonebook) {
        if(!phonebook.getContacts().isEmpty()){
            for (Contact contact : phonebook.getContacts()) {
                System.out.println("====================================");
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact Number: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
            }
        }
        else{
            System.out.println("Empty Phonebook");
            System.out.println("====================================");
        }
    }

    public void getDetailsByName(String name, Phonebook phonebook){
        if(!phonebook.getContacts().isEmpty()){
            for (Contact contact : phonebook.getContacts()) {
                if (contact.getName().equals(name)) {
                    System.out.println("====================================");
                    System.out.println("Name: " + contact.getName());
                    System.out.println("Contact Number: " + contact.getContactNumber());
                    System.out.println("Address: " + contact.getAddress());
                }
            }
            System.out.println("====================================");
        }
        else{
            System.out.println("Empty Phonebook");
            System.out.println("====================================");
        }
    }
}