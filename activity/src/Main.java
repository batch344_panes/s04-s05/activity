public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        phonebook.setContacts(new Contact("John Doe", "+639152468596", "my home in Quezon City"));
        phonebook.setContacts(new Contact("John Doe", "+639228547963", "my office in Makati City"));
        phonebook.setContacts(new Contact("Jane Doe", "+639162148573", "my home in Caloocan City"));
        phonebook.setContacts(new Contact("Jane Doe", "+639173698541", "my home in Pasay City"));

        Phonebook emptyPhonebook = new Phonebook();

        ContactActions contactActions = new ContactActions();
        contactActions.getDetails(emptyPhonebook);
        contactActions.getDetails(phonebook);

        contactActions.getDetailsByName("John Doe", phonebook);

        PhoneBookActions phoneBookActions = new PhoneBookActions();

        phoneBookActions.getDetails(emptyPhonebook);
        phoneBookActions.getDetails(phonebook);
    }
}