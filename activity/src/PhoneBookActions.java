import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class PhoneBookActions implements Actions {
    @Override
    public void getDetails(Phonebook phonebook) {
        if(!phonebook.getContacts().isEmpty()){
            Set<String> distinctNames = new HashSet<>();
            for (Contact contact : phonebook.getContacts()) {
                distinctNames.add(contact.getName());
            }

            HashMap<String, ArrayList<Contact>> filteredContactsMap = new HashMap<>();
            for (String name : distinctNames) {
                ArrayList<Contact> filteredContacts = new ArrayList<>();

                for (Contact contact : phonebook.getContacts()) {
                    if (contact.getName().equals(name)) {
                        filteredContacts.add(contact);
                    }
                }

                filteredContactsMap.put(name, filteredContacts);
            }

            for (String name : distinctNames) {
                ArrayList<Contact> filteredContacts = filteredContactsMap.get(name);

                System.out.println(name);
                System.out.println("------------------------------------------------");
                System.out.println(name + " has the following registered numbers: ");
                for (Contact contact : filteredContacts) {
                    System.out.println(contact.getContactNumber());
                }

                System.out.println("------------------------------------------------");
                System.out.println(name + " has the following registered addresses: ");
                for (Contact contact : filteredContacts) {
                    System.out.println(contact.getAddress());
                }
                System.out.println("================================================");
            }
        }
        else{
            System.out.println("Empty Phonebook");
            System.out.println("====================================");
        }
    }
}